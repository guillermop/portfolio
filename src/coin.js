/* coin.js
 *
 * Copyright 2021 Guillermo Peña
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

const { GObject, Gtk, GLib, Gio, Adw } = imports.gi

var Coin = new GObject.registerClass({
    Properties: {
        'name': GObject.ParamSpec.string(
            'name',
            'Coin name', 'Coin name in string',
            GObject.ParamFlags.READWRITE | GObject.ParamFlags.CONSTRUCT,
            null),
        'symbol': GObject.ParamSpec.string(
            'symbol',
            'Coin symbol', 'Coin symbol in string',
            GObject.ParamFlags.READWRITE | GObject.ParamFlags.CONSTRUCT,
            null)
    },

}, class Coin extends GObject.Object {
    _init({ id, name, symbol, current_price: currentPrice, price_change_percentage_24h: change }, holding) {
        super._init({});
        this.id = id;
        this.name = name;
        this.symbol = symbol;
        this.currentPrice = currentPrice;
        this.holding = holding;
        this.change = change;
        this.total = holding * currentPrice;
        this.gains = holding*(currentPrice - currentPrice/(1+change/100));
    }
});
