/* preferences.js
 *
 * Copyright 2021 Guillermo Peña
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

const { GObject, Gio, Adw } = imports.gi;
const { Settings } = imports.settings;

var Preferences = GObject.registerClass({
    GTypeName: 'Preferences',
    Template: 'resource:///com/gitlab/guillermop/Portfolio/preferences.ui',
    InternalChildren: [
        'darkSwitch',
        'secondsSpin'
    ]
}, class Preferences extends Adw.PreferencesWindow {
    _init(settings) {
        super._init({});
        Settings.get_default().bind(
            "dark-theme",
            this._darkSwitch,
            "active",
            Gio.SettingsBindFlags.DEFAULT
        );
        Settings.get_default().bind(
            "pull-interval",
            this._secondsSpin,
            "value",
            Gio.SettingsBindFlags.DEFAULT
        );
    }
});

