/* settings.js
 *
 * Copyright 2021 Guillermo Peña
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

const { GObject, Gio } = imports.gi

let instance = null;

var Settings = new GObject.registerClass({
    Properties: {
        'dark-theme': GObject.ParamSpec.boolean(
            'dark-theme',
            'Dark Theme',
            '',
            GObject.ParamFlags.READWRITE,
            false
        ),
        'pull-interval': GObject.ParamSpec.int(
            'pull-interval',
            'Pull Interval',
            '',
            GObject.ParamFlags.READWRITE,
            1,
            60,
            1
        ),
    },

}, class Settings extends Gio.Settings {
    _init(schema_id) {
        super._init({schema_id});
        instance = this;
        return instance;
    }
    get dark_theme() {
        return this.get_boolean('dark-theme');
    }
    get pull_interval() {
        return this.get_int('pull-interval');
    }
    static get_default() {
        return instance;
    }
});

