/* dialog.js
 *
 * Copyright 2021 Guillermo Peña
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

const { GObject, Gtk, Adw, Gio, Gdk, Pango } = imports.gi;

var Dialog = GObject.registerClass({
    GTypeName: 'Dialog',
    Template: 'resource:///com/gitlab/guillermop/Portfolio/dialog.ui',
    Signals: {
        'response': { param_types: [GObject.TYPE_INT] }
    },
    InternalChildren: ['coinEntry', 'holdingEntry', 'coinCompletion', 'comboRow']
}, class Dialog extends Adw.Window {
    _init(params) {
        const { supportedCoins, ...other } = params
        super._init(other);
        this._supportedCoins = supportedCoins;

        this._store = new Gtk.StringList();
        for (let i = 0; i < supportedCoins.length; i++) {
            this._store.append(supportedCoins[i].name)
        }

        this._coinEntry.set_model(this._store);
        this._coinEntry.set_enable_search(true);

        const expresion = new Gtk.ClosureExpression(String, (obj) => {
            return obj.get_string();
        }, []);

        const setEllipsize = (widget) => {
            let child = widget.get_first_child();
            while(child) {
                if (child.label) {
                    child.ellipsize = Pango.EllipsizeMode.END;
                }
                setEllipsize(child);
                child = child.get_next_sibling();
            }
        }
        setEllipsize(this._coinEntry.get_first_child());

        this._coinEntry.set_expression(expresion);

    }
    onKeyPressed(event, keyval, keycode, state) {
        if (keyval == Gdk.KEY_Escape) {
            this.emit('response', Gtk.ResponseType.CANCEL)
            return true
        }
        return false
    }
    get record() {
        const name = this._coinEntry.get_selected_item().get_string();
        const { id } = this._supportedCoins.find((element) => {
            return element.name === name;
        });

        return {
            'id': id,
            'holding': this._holdingEntry.text
        }
    }
    onSaveButtonClicked() {
        this.emit('response', Gtk.ResponseType.OK)
    }
});

