/* window.js
 *
 * Copyright 2021 Guillermo Peña
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

const { GObject, Gtk, Adw, Gio, Gdk } = imports.gi;
const { CoinGecko } = imports.api;
const { Store } = imports.store;
const { Dialog } = imports.dialog;
const { Coin } = imports.coin;
const { Timer } = imports.timer;
const { Settings } = imports.settings;
const { Row } = imports.row;

const VS_CURRENCY = 'usd'

var PortfolioWindow = GObject.registerClass({
    Properties: {
        'empty': GObject.ParamSpec.boolean(
            'empty',
            'Empty',
            '',
            GObject.ParamFlags.READWRITE,
            true
        ),
        'selection-mode': GObject.ParamSpec.boolean(
            'selection-mode',
            'Selection mode',
            '',
            GObject.ParamFlags.READWRITE,
            false
        ),
    },
    GTypeName: 'PortfolioWindow',
    Template: 'resource:///com/gitlab/guillermop/Portfolio/window.ui',
    InternalChildren: [
        'mainStack',
        'listStack',
        'headerStack',
        'listBox',
        'balanceLabel',
        'searchBar',
        'searchEntry',
        'selectionButton',
        'toastOverlay',
        'deleteButton'
    ]
}, class PortfolioWindow extends Adw.ApplicationWindow {
    _init(application) {
        super._init({ application });

        this._model = new Gio.ListStore();

        this._filter = new Gtk.CustomFilter();
        this._filter.set_filter_func(({name, symbol}) => {
            const filterText = this._searchEntry.text.trim().toLowerCase();
            if (!filterText)
                return true;
            return name.toLowerCase().includes(filterText) ||
                   symbol.toLowerCase().includes(filterText);
        });

        this._filteredModel = new Gtk.FilterListModel({
            model: this._model,
            filter: this._filter
        });

        this._items_changed_handler_id = this._model.connect('items-changed', () => {
            this.empty = this._model.get_n_items() === 0;
            let total = 0;
            for (let i = 0; i < this._model.get_n_items(); i++) {
                total += this._model.get_item(i).total;
            }
            const numberFormat = new Intl.NumberFormat(
                undefined,
                {
                    style: 'currency',
                    currency: 'USD',
                    currencyDisplay: 'code',
                }
            );
            this._balanceLabel.label = numberFormat.format(total);
        });

        this._filteredModel.connect('items-changed', () => {
            this._listStack.set_visible_child_name(
                this._filteredModel.get_n_items() == 0 ? 'empty' : 'list'
            );
        });

        this._listBox.bind_model(this._filteredModel, (coin) => {
            let row = new Row(coin);
            this.bind_property(
                'selection-mode',
                row,
                'selection-mode',
                GObject.BindingFlags.SYNC_CREATE
            );
            return row;
        });


        this._api = new CoinGecko();
        this._store = new Store();
        this._holdings = this._store.holdings;

        this._coins_markets = []
        this._api.coinsMarkets({
            vs_currency: VS_CURRENCY
        })
        .then(({ statusCode, data }) => {
            data.forEach(element => {
                const { id, name } = element
                this._coins_markets.push({ id, name })
            })
        });

        this._coins_list = []
        this._api.coinsList()
        .then(({ statusCode, data }) => {
            this._coins_list = [...data];
        });

        this._addAction('add', () => {
            let dialog = new Dialog({
                title: 'Add coin',
                transient_for: this,
                supportedCoins: [
                    ...this._coins_markets,
                    ...this._coins_list.filter(element => !this._coins_markets.find(({id}) => element.id === id))
                ]
            })
            dialog.connect("response", (d, response) => {
                if (response == Gtk.ResponseType.OK) {
                    const { id, holding } = dialog.record;
                    if (id) {
                        this._holdings[id] = (this._holdings[id] || 0) + parseFloat(holding);
                        this._store.save({'version': 1, 'holdings': this._holdings});
                        this.update();
                    }
                }
                dialog.destroy();
            });
            dialog.present()

        });

        this._addAction('search', () => {
            const mode = this._searchBar.get_search_mode();
            if (mode && !this._searchEntry.get_focus_child())
                this._searchEntry.grab_focus();
            else
                this._searchBar.set_search_mode(!mode);
        }, true);

        this._addAction('select-all', () => {
            if (!this.selection_mode)
                this.selection_mode = true;
            this._listBox.select_all();
        }, true);

        this._addAction('select-none', () => {
            this._listBox.unselect_all()
        }, true);

        this._addAction('undo', () => {
            while(this._toDelete.length) {
                const coin = this._toDelete.pop();
                this._holdings[coin.id] = (this._holdings[coin.id] || 0) + parseFloat(coin.holding);
            }
        });

        this._toDelete = [];
        this.update();

        this._timer = new Timer();
        this._timer.connect('timeout', () => {
            this.update();
        });

        Settings.get_default().connect('changed::pull-interval', () => {
            this._timer.reset();
        });
    }
    get empty() {
        if (this._empty === undefined)
            this._empty = true;
        return this._empty;
    }
    set empty(value) {
        if (this.empty === value)
            return;
        this._empty = value;
        this.notify('empty');
    }
    get selection_mode() {
        if (this._selection_mode === undefined)
            this._selection_mode = false;
        return this._selection_mode;
    }
    set selection_mode(value) {
        if (this.selection_mode === value)
            return;
        this._selection_mode = value;
        this.notify('selection-mode');
    }
    _addAction(name, callback, bind=false) {
        const action = new Gio.SimpleAction({name})
        action.connect("activate", callback)
        if (bind)
            this.bind_property(
                "empty",
                action,
                "enabled",
                GObject.BindingFlags.INVERT_BOOLEAN | GObject.BindingFlags.SYNC_CREATE
            )
        this.add_action(action)
    }
    update() {
        const ids = Object.keys(this._holdings);
        if (ids.length === 0) return;
        this._api.coinsMarkets({
         vs_currency: VS_CURRENCY,
         ids: ids.join(',')
         })
        .then(response => {
            const { statusCode, data } = response
            GObject.signal_handler_block(this._model, this._items_changed_handler_id);
            this._model.remove_all();
            GObject.signal_handler_unblock(this._model, this._items_changed_handler_id);
            data.forEach((coin) => {
                const temp = new Coin(coin, this._holdings[coin.id]);
                this._model.insert_sorted(temp, (a, b) => a.total < b.total);
            });
        })
        .catch((error) => {
            log(`error: ${error}`);
        })
        .finally(() => {
            this._timer.start();
        });
    }
    onSelectionModeChanged() {
        this._listBox.unselect_all()
        this._headerStack.set_visible_child_name(
            this.selection_mode ? 'selection' : 'default'
        );
    }
    onCancelButtonClicked() {
        this.selection_mode = false;
    }
    onDeleteButtonClicked() {
        const coins = this._listBox.get_selected_rows().reduce((acc, cur) => {
            return [...acc, this._filteredModel.get_item(cur.get_index())];
        }, []);

        coins.forEach(coin => {
            delete this._holdings[coin.id];
            const [ok, position] = this._model.find(coin);
            if (ok)
                this._model.remove(position);
        });

        this._toDelete = [...this._toDelete, ...coins];

        let message = `${this._toDelete.length} coins deleted`;
        if (this._toDelete.length == 1) {
            message = `"${this._toDelete[0].name}" deleted`;
        }

        if (this._undoToast) {
            this._undoToast.title = message;
            return
        }

        this._undoToast = new Adw.Toast({
            title: message,
            button_label: 'Undo',
            action_name: 'win.undo',
            priority: Adw.ToastPriority.HIGH
        });
        this._undoToast.connect('dismissed', () => {
            this._undoToast = null;
            this._toDelete = [];
            this._store.save({'version': 1, 'holdings': this._holdings});
            this._selectionButton.grab_focus();
            this.update();
        });
        this._toastOverlay.add_toast(this._undoToast);
    }
    onSelectedRowsChanged() {
        for (let i = 0; i < this._filteredModel.get_n_items(); i++) {
            const row = this._listBox.get_row_at_index(i);
            row.selected = row.is_selected();
        }
        const selected = this._listBox.get_selected_rows().length;
        let label = '';
        if (selected !== 0) {
            label = `${selected} selected coin(s)`;
            this._deleteButton.sensitive = true;
        } else {
            label = 'Click on items to select them';
            this._deleteButton.sensitive = false;
        }
        this._selectionButton.label = label;
    }
    onSearchModeChanged() {
        if (!this._searchBar.get_search_mode())
            this._listBox.grab_focus();
    }
    onEmptyChanged() {
        let visible = 'default';
        if (this.empty) {
            visible = 'empty'
            this._searchBar.set_search_mode(false);
            this.selection_mode = false;
        }
        this._mainStack.set_visible_child_name(visible);
    }
    onSearchEntryChanged() {
        this._filter.changed(Gtk.FilterChange.DIFFERENT);
    }
    onListBoxClick(gesture, n, x, y) {
        const state = gesture.get_current_event_state();
        if (!this.selection_mode) {
            if (state & Gdk.ModifierType.CONTROL_MASK || state & Gdk.ModifierType.SHIFT_MASK)
                this.selection_mode = true;
            else
                return;
        }
        const row = this._listBox.get_row_at_y(y);
        if (!row) return;
        if (row.selected) {
            this._listBox.unselect_row(row);
            gesture.set_state(Gtk.EventSequenceState.CLAIMED);
        }

    }
});

