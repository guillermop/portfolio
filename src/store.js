/* store.js
 *
 * Copyright 2021 Guillermo Peña
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

const { GLib, Gio } = imports.gi
const ByteArray = imports.byteArray;

var Store = class {
    constructor() {
        this._filename = GLib.build_filenamev([
            GLib.get_user_data_dir(),
            'portfolio',
            'holdings.json'
        ]);
        GLib.mkdir_with_parents(GLib.path_get_dirname(this._filename), 0o744);
    }
    get holdings() {
        let status, buffer;
        let file = Gio.File.new_for_path(this._filename);
        try {
            [status, buffer] = file.load_contents(null);
            if (!status) return {};
            if (buffer instanceof Uint8Array) {
                buffer = ByteArray.toString(buffer);
            } else {
                buffer = buffer.toString();
            }
            return JSON.parse(buffer).holdings;
       } catch (e) {
            return {};
        }
    }
    save(data) {
        let file = Gio.File.new_for_path(this._filename);
        let buffer = JSON.stringify(data);
        try {
            return file.replace_contents(buffer, null, false, 0, null)[0];
        } catch (e) {
            return false;
        }
    }
};
