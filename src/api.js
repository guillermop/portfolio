/* api.js
 *
 * Copyright 2021 Guillermo Peña
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

const GLib = imports.gi.GLib;
const Soup = imports.gi.Soup;

TIMEOUT = 5;

var CoinGecko = class {
    constructor() {
        this._session = new Soup.Session({ user_agent : 'portfolio/' + pkg.version, timeout: 5 });
        this._baseUrl = 'https://api.coingecko.com/api/v3/';
    }
    _fetch(url) {
        let msg = Soup.Message.new('GET', url);
        return new Promise((resolve, reject) => {
            this._session.queue_message(msg, (session, message) => {
                try {
                    const { status_code, response_body } = message;
                    resolve({
                        statusCode: status_code,
                        data: JSON.parse(response_body.data)
                    });
                } catch (e) {
                    reject(e);
                }
            })
        });
    }
    ping() {
        const url = `${this._baseUrl}ping`;
        return this._fetch(url);
    }
    coinsMarkets(params={}) {
        const { vs_currency='usd', ids, limit=250} = params;
        let url = `${this._baseUrl}coins/markets?vs_currency=${vs_currency}`
        if (ids) {
            url += `&ids=${Soup.URI.encode(ids, ',')}`;
        } else if (limit) {
            url += `&limit=${limit}`;
        }
        return this._fetch(url);
    }
    coinsList() {
        const url = `${this._baseUrl}coins/list?include_platform=false`;
        return this._fetch(url);
    }
};



