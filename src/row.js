/* row.js
 *
 * Copyright 2021 Guillermo Peña
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

const { GObject, Gtk, Gio } = imports.gi;

var Row = GObject.registerClass({
    Properties: {
        'selection-mode': GObject.ParamSpec.boolean(
            'selection-mode',
            'Selection mode',
            'Whether the row is in selection mode',
            GObject.ParamFlags.READWRITE,
            false
        ),
        'selected': GObject.ParamSpec.boolean(
            'selected',
            'Selected',
            'Whether the row is selected',
            GObject.ParamFlags.READWRITE,
            false
        )
    },
    GTypeName: 'Row',
    Template: 'resource:///com/gitlab/guillermop/Portfolio/row.ui',
    InternalChildren: ['nameLabel', 'holdingLabel', 'priceLabel', 'changeLabel', 'checkButton']
}, class Row extends Gtk.ListBoxRow {
    _init(coin) {
        super._init({});
        this._coin = coin;
        this.update();
    }
    update() {
        const numberFormat = new Intl.NumberFormat(undefined, { style: 'currency', currency: 'USD', currencyDisplay: 'code' });
        const { name, symbol, holding, currentPrice, change, total, gains } = this._coin;
        this._nameLabel.label = name;
        this._holdingLabel.label = `${holding} ${symbol.toUpperCase()} (${numberFormat.format(total)})`;
        this._priceLabel.label = `${numberFormat.format(currentPrice)}`;
        const changeStyle = this._changeLabel.get_style_context();
        changeStyle.remove_class("green");
        changeStyle.remove_class("red");
        if (change >= 0) {
            changeStyle.add_class("green");
        } else {
            changeStyle.add_class("red");
        }
        this._changeLabel.label = `${numberFormat.format(gains)} (${change.toFixed(2)}%)`;
    }
    onSelectedChanged() {
        if (!this.selection_mode) return;
        if (this.selected) {
            if (!this.is_selected()) {
                this.get_parent().select_row(this)
            }
        }
        else if(!this._checkButton.get_active()) {
            this.get_parent().unselect_row(this)
        }
    }
    onSelectionModeChanged() {
        this.set_activatable(this.selection_mode);
        this.set_selectable(this.selection_mode);
        this._checkButton.active = false;
    }
});

