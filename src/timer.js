/* timer.js
 *
 * Copyright 2021 Guillermo Peña
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

const { GObject, GLib } = imports.gi;
const { Settings } = imports.settings;

var Timer = new GObject.registerClass({
    Signals: {
        'timeout': {},
    }
}, class Timer extends GObject.Object {
    _init() {
        super._init({});
        this._timeoutId = 0;
    }
    start() {
        const seconds = Settings.get_default().pull_interval;
        this._timeoutId = GLib.timeout_add_seconds(
            GLib.PRIORITY_HIGH,
            seconds, () => {
                this._timeoutId = 0;
                this.emit('timeout');
                return false;
            }
        );
    }
    stop() {
        if (this._timeoutId === 0)
            return
        GLib.Source.remove(this._timeoutId);
        this._timeoutId = 0
    }
    reset() {
        this.stop();
        this.start();
    }
});

