/* application.js
 *
 * Copyright 2021 Guillermo Peña
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

const { Gtk, Gio, GObject, GLib, Gdk, Adw } = imports.gi;
const { PortfolioWindow } = imports.window;
const { Settings } = imports.settings;
const { Preferences } = imports.preferences;

var Application = GObject.registerClass(class Application extends Gtk.Application {
    _init() {
        super._init({
            application_id: pkg.name,
            flags: Gio.ApplicationFlags.FLAGS_NONE,
        });
        GLib.set_application_name('Portfolio');
        const settings = new Settings(pkg.name);
        settings.connect('changed::dark-theme', this._onDarkThemeChanged)
    }
    vfunc_startup() {
        super.vfunc_startup();
        Adw.init();

        const styleProvider = new Gtk.CssProvider();
        styleProvider.load_from_resource('/com/gitlab/guillermop/Portfolio/main.css');
        Gtk.StyleContext.add_provider_for_display(
            Gdk.Display.get_default(),
            styleProvider,
            Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION
        );

        this._addAction('quit', () => {
            this.quit();
        });
        this._addAction('preferences', () => {
            const preferences = new Preferences(this.settings)
            preferences.set_transient_for(this.activeWindow)
            preferences.present()
        });
        this.set_accels_for_action("app.preferences", ["<Ctrl>comma"]);
        this.set_accels_for_action("app.quit", ["<Ctrl>Q"]);
        this.set_accels_for_action("win.add", ["<Ctrl>N"]);
        this.set_accels_for_action("win.search", ["<Ctrl>F"]);
        this.set_accels_for_action("win.select-all", ["<Ctrl>A"]);
        this._onDarkThemeChanged();
    }
    vfunc_activate() {
        let activeWindow = this.activeWindow;
        if (!activeWindow) {
            activeWindow = new PortfolioWindow(this);
            if (pkg.name.endsWith('Devel')) {
                activeWindow.get_style_context().add_class('devel');
            }
        }
        activeWindow.present();
    }
    _addAction(name, callback) {
        const action = new Gio.SimpleAction({name})
        action.connect("activate", callback)
        this.add_action(action)
    }
    _onDarkThemeChanged() {
        let color = Adw.ColorScheme.PREFER_LIGHT
        if (Settings.get_default().dark_theme)
            color = Adw.ColorScheme.PREFER_DARK
        Adw.StyleManager.get_default().set_property(
            'color-scheme',
            color
        )
    }
});
