# Portfolio

Portfolio is a simple cryptocurrency portfolio tracker

<div align="center">
![screenshot](data/screenshots/screenshot1.png)
</div>

## Building

You can build Portfolio with Gnome Builder, just clone the project and hit the run button.
