#!/usr/bin/env python3

from os import environ, path, makedirs, sep
from subprocess import call
import sys

prefix = environ.get('MESON_INSTALL_PREFIX', '/usr/local')
destdir = environ.get('DESTDIR', '')
datadir = sys.argv[1]
bindir = path.normpath(destdir + sep + sys.argv[2])
pkgdatadir = sys.argv[3]
application_id = sys.argv[4]

if not path.exists(bindir):
  makedirs(bindir)

src = path.join(pkgdatadir, application_id)
dest = path.join(bindir, 'portfolio')
call(['ln', '-s', '-f', src, dest])

# Package managers set this so we don't need to run
if not destdir:
    print('Updating icon cache...')
    call(['gtk-update-icon-cache', '-qtf', path.join(datadir, 'icons', 'hicolor')])

    print('Updating desktop database...')
    call(['update-desktop-database', '-q', path.join(datadir, 'applications')])

    print('Compiling GSettings schemas...')
    call(['glib-compile-schemas', path.join(datadir, 'glib-2.0', 'schemas')])


